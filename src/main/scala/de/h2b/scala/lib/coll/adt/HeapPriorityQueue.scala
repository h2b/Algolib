/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

import scala.reflect.ClassTag

/**
 * This abstract class represents a priority queue of generic keys.
 * It supports the usual ''insert'' and ''delete'' operations, along with
 * methods for peeking at the highest-priority key, testing if the priority
 * queue is empty, and iterating through the keys.
 *
 * The super trait {@code PriorityQueue} supports both minimum and maximum
 * priority-key implementations by means of the {@code ht} method.
 *
 * This implementation uses a binary heap based on a resizing array. Enqueuing
 * and dequeuing operations take logarithmic amortized time.
 * The ''peek'', ''size'', and ''isEempty'' operations take constant time.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see <a href="http://algs4.cs.princeton.edu/24pq">Section 2.4</a> of
 *      <i>Algorithms, 4th Edition</i>
 * @author h2b
 */
abstract class HeapPriorityQueue [Key : Ordering : ClassTag] extends ResizingArrayPriorityQueue[Key] {

  /*
   * Note that we need a capacity of at least n+1, since a(0) is unused and
   * the actual keys are in a(1), ..., a(n).
   */
  override protected def resize (capacity: Int) = {
    assert(capacity>n)
    val aa = Array.ofDim[Key](capacity)
    for (i <- 1 to n) aa(i) = a(i)
    a = aa
  }

	/**
	 * Bottom-up reheapify.
	 *
	 * @param k
	 */
	private def swim (k: Int) = {
	  var j = k
		while (j>1 && ht(a(j),a(j/2))) {
		  swap(j, j/2)
		  j /= 2
		}
  }

	/**
	 * Top-down reheapify.
	 *
	 * @param k
	 */
	private def sink (k: Int): Unit = {
		var j = k
		while (2*j<=n) {
		  var i = 2*j
		  if (i<n && ht(a(i+1),a(i))) i += 1
		  if (!ht(a(i),a(j))) return
		  swap(i, j)
		  j = i
		}
	}

  def enqueue (item: Key): Unit = {
    if (n+1==a.length) resize(a.length*2)
    //append new item at the end of the array and swim up
    n += 1
    a(n) = item
    swim(n)
  }

  def peek: Key = {
    if (isEmpty) throw new NoSuchElementException("Priority queue underflow")
    a(1)
  }

  def dequeue (): Key = {
    if (isEmpty) throw new NoSuchElementException("Priority queue underflow")
    //exchange first item with the last, delete it and let new first item sink down
    val item = a(1)
    swap(1, n)
    a(n) = _: Key //to avoid loitering
    n -= 1
    sink(1)
    if (n+1==a.length/4) resize(a.length/2)
    item
  }

	override def toString = super.toString + "Heap"

}