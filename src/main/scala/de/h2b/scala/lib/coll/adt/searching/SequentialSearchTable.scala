/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt.searching

import de.h2b.scala.lib.coll.adt.Queue

/**
 * This class implements an unordered symbol table of generic key/value pairs.
 *
 * This implementation uses a singly-linked list and sequential search.
 * It relies on the {@code equals} method to test whether two keys are equal.
 * It does not call either the {@code compare} or {@code hashCode} method.
 *
 * The ''put'' and ''delete'' operations take linear time; the ''get'' and
 * ''contains'' operations takes linear time in the worst case. The ''size'',
 * and ''is-empty'' operations take constant time. Construction takes constant
 * time.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      [[http://algs4.cs.princeton.edu]]
 * @see [[http://algs4.cs.princeton.edu/31elementary Section 3.1]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
class SequentialSearchTable [Key, Value] private extends BasicSymbolTable[Key, Value] {

  import SequentialSearchTable._

  private var first: Node[Key, Value] = null

  private var n = 0 //numer of items

	def update (key: Key, value: Value): Unit = {
	  //search for key and update value if found or grow table else
    var node = first
    while (node!=null) {
      if (node.key==key) { node.value = value; return }
      node = node.next
    }
    first = new Node(key, value, first)
    n += 1
  }

	def apply (key: Key): Value = {
    //search for key and return associated value
    var node = first
    while (node!=null) {
      if (node.key==key) return node.value
      node = node.next
    }
    throw new NoSuchElementException(s"not found: $key")
  }

	def delete (key: Key): Unit = {
	  //search for key and delete it
	  var node = first
	  if (node!=null) while (node.next!=null) {
	    if (node.next.key==key) { node.next = node.next.next;  n -= 1; return }
	    node = node.next
	  }
    throw new NoSuchElementException(s"not found: $key")
  }

  def size: Int = n

  def keys (): Iterable[Key] = {
    val q = Queue[Key]()
    var node = first
    while (node!=null) { q.enqueue(node.key); node = node.next }
    q
  }

  override def toString = "SequentialSearchTable"

}

object SequentialSearchTable {

  private class Node [Key, Value] (val key: Key, var value: Value, var next: Node[Key, Value])

  def apply [Key, Value] () = new SequentialSearchTable[Key, Value]()

}