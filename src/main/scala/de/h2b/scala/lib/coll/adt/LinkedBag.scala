/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

/**
 * Provides a bag (or multiset) of generic items, supporting insertion and
 * iterating over the items in arbitrary order.
 * <p>
 * This implementation uses a singly-linked list.
 * The {@code add}, {@code isEmpty}, and {@code size} operations
 * take constant time. Iteration takes time proportional to the number of items.
 * <p>
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see <a href="http://algs4.cs.princeton.edu/13stacks">Section 1.3</a> of
 *      <i>Algorithms, 4th Edition</i>
 * @author h2b
 */
class LinkedBag[A] private extends Bag[A] {

  import LinkedBag._

  private var n = 0 //number of items

  private var first: Node[A] = null

	override def isEmpty: Boolean = first==null

  override def size: Int = n

  def add (item: A) = {
    first = new Node(item, first)
    n += 1
  }

  def iterator = new Iterator[A] {
    private var current = first
    def hasNext = current!=null
    def next = {
      if (!hasNext) throw new NoSuchElementException
      val item = current.item
      current = current.next
      item
    }
  }

}

object LinkedBag {

  private class Node[A] (val item: A, val next: Node[A])

  def apply [A] () = new LinkedBag[A]

}