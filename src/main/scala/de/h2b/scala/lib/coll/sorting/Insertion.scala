/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.sorting

/**
 * Implements insertion sort for sorting arrays in place.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see [[http://algs4.cs.princeton.edu/21elementary Section 2.1]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
object Insertion extends Sort {

  def sort [E : Ordering] (a: Array[E]): Unit = {
    val n = a.length
    for (i <- 1 until n) {
      //insert a(i) among a(i-1), a(i-2), ...
      var j = i
      while (j>0 && lt(a(j),a(j-1))) {
        swap(a, j, j-1)
        j -= 1
      }
    }
    assert(isSorted(a))
  }

  override def toString = "InsertionSort"

}