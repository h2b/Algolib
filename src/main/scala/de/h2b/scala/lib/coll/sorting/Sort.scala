/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.sorting

/**
 * Defines a sorting trait to be implemented by various objects for sorting
 * arrays in place using different algorithms.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see [[http://algs4.cs.princeton.edu/21elementary Section 2.1]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
trait Sort {

	/**
   * Sorts array in place in increasing order according to an implicitly or
   * explicitly given ordering.
   *
	 * @param a the array to be sorted
	 * @tparam E element type (context bound {@code Ordering})
   * @usecase def sort [E] (a: Array[E])
	 */
	def sort [E : Ordering] (a: Array[E]): Unit

  /**
   * Tests whether the array elements are in increasing order according to the
   * implicitly or explicitly given ordering.
   *
   * @param a the array to be tested
   * @tparam E element type (context bound {@code Ordering})
   * @return {@code true} if a is sorted, {@code false} otherwise
   * @usecase def isSorted [E] (a: Array[E])
   */
	def isSorted [E : Ordering] (a: Array[E]): Boolean = {
		for (i <- 1 until a.length) if (lt(a(i),a(i-1))) return false
    true
	}

	/**
	 * @param v
	 * @param w
	 * @param ord
	 * @return v<w
	 */
	protected def lt [E] (v: E, w: E) (implicit ord: Ordering[E]) = ord.lt(v, w)

	/**
   * Swaps a,,i,, and a,,j,,.
   *
	 * @param a
	 * @param i
	 * @param j
	 */
	protected def swap [E] (a: Array[E], i: Int, j: Int) = {
    require(0<=i && i<a.length && 0<=j && j<a.length)
		val t = a(i)
		a(i) = a(j)
		a(j) = t
	}

}