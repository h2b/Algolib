/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

import scala.reflect.ClassTag

abstract class ResizingArrayPriorityQueue [Key : ClassTag] extends PriorityQueue[Key] {

	protected var a = Array.ofDim[Key](2)

	protected var n = 0 //numer of items

  override def size: Int = n

  protected def resize (capacity: Int) = {
    assert(capacity>=n)
    val aa = Array.ofDim[Key](capacity)
    for (i <- 0 until n) aa(i) = a(i)
    a = aa
  }

	/**
   * Swaps a,,i,, and a,,j,,.
   *
	 * @param i
	 * @param j
	 */
	protected def swap (i: Int, j: Int) = {
    require(0<=i && i<a.length && 0<=j && j<a.length)
		val t = a(i)
		a(i) = a(j)
		a(j) = t
	}

	override def toString = super.toString + "ResizingArray"

}

abstract class UnorderedResizingArrayPriorityQueue [Key : Ordering : ClassTag] extends ResizingArrayPriorityQueue[Key] {

  def enqueue (item: Key): Unit = {
    if (n==a.length) resize(a.length*2)
    a(n) = item
    n += 1
  }

  def peek: Key = {
    if (isEmpty) throw new NoSuchElementException("Priority queue underflow")
    a(highestIndex)
  }

  private def highestIndex = {
    var h = 0
    for (i <- 1 until n) if (ht(a(i),a(h))) h = i
    h
  }

  def dequeue (): Key = {
    if (isEmpty) throw new NoSuchElementException("Priority queue underflow")
    swap(highestIndex, n-1)
    val item = a(n-1)
    a(n-1) = _: Key //to avoid loitering
    n -= 1
    if (n>0 && n==a.length/4) resize(a.length/2)
    item
  }

	override def toString = super.toString + "Unordered"

}

abstract class OrderedResizingArrayPriorityQueue [Key : Ordering : ClassTag] extends ResizingArrayPriorityQueue[Key] {

	def enqueue (item: Key): Unit = {
    if (n==a.length) resize(a.length*2)
    a(n) = item
    //insert a(n) among a(n-1), a(n-2), ...
    var j = n
    while (j>0 && ht(a(j-1),a(j))) {
    	swap(j, j-1)
    	j -= 1
    }
    n += 1
  }

	def peek: Key = {
    if (isEmpty) throw new NoSuchElementException("Priority queue underflow")
    a(n-1)
  }

	def dequeue (): Key = {
    if (isEmpty) throw new NoSuchElementException("Priority queue underflow")
    val item = a(n-1)
    a(n-1) = _: Key //to avoid loitering
    n -= 1
    if (n>0 && n==a.length/4) resize(a.length/2)
    item
  }

	override def toString = super.toString + "Ordered"

}
