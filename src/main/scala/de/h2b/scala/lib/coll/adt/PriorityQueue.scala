/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

import scala.collection.Iterable
import scala.reflect.ClassTag

/**
 * This trait represents a priority queue of generic keys.
 * It supports the usual ''insert'' and ''delete'' operations, along with
 * methods for peeking at the highest-priority key, testing if the priority
 * queue is empty, and iterating through the keys.
 *
 * This trait supports both minimum and maximum priority-key implementations by
 * means of the {@code ht} method.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see <a href="http://algs4.cs.princeton.edu/24pq">Section 2.4</a> of
 *      <i>Algorithms, 4th Edition</i>
 * @author h2b
 */
trait PriorityQueue [Key] extends Iterable[Key] {

  def enqueue (item: Key): Unit

  /**
   * Returns the item with highest priority from this queue without consuming
   * it.
   *
   * @return the highest-priority item from this queue
   * @throws NoSuchElementException if this priority queue is empty
   */
  def peek: Key

  /**
   * Removes and returns the item with highest priority from this queue.
   *
   * @return the highest-priority item from this queue
   * @throws NoSuchElementException if this priority queue is empty
   */
  def dequeue (): Key

  override def isEmpty: Boolean = size==0

  def size: Int

  /**
   * Returns an iterator that iterates over the items in this queue in FIFO order
   * with (!) consuming them.
   * .
   * @return the iterator
   */
  def iterator = new Iterator[Key] {
    def hasNext = !PriorityQueue.this.isEmpty //don't use Iterator's own isEmpty
    def next = {
      if (!hasNext) throw new NoSuchElementException
      dequeue()
    }
  }

  /**
   * @param a
   * @param b
   * @return a has higher priority than b
   */
  protected def ht (a: Key, b: Key)  (implicit ord: Ordering[Key]): Boolean

  override def toString = "PriorityQueue"

}

object PriorityQueue {

  trait PriorityOrder

  trait MaxPQ [Key] extends PriorityQueue[Key] with PriorityOrder {
    protected def ht (a: Key, b: Key) (implicit ord: Ordering[Key]): Boolean = ord.gt(a, b)
    override def toString = super.toString + "Max"
  }

  trait MinPQ [Key] extends PriorityQueue[Key] with PriorityOrder {
    protected def ht (a: Key, b: Key) (implicit ord: Ordering[Key]): Boolean = ord.lt(a, b)
    override def toString = super.toString + "Min"
  }

  class MaxPqWithUnorderedResizingArray [Key : Ordering : ClassTag] extends UnorderedResizingArrayPriorityQueue[Key] with MaxPQ[Key]

  class MaxPqWithOrderedResizingArray [Key : Ordering : ClassTag] extends OrderedResizingArrayPriorityQueue[Key] with MaxPQ[Key]

  class MinPqWithUnorderedResizingArray [Key : Ordering : ClassTag] extends UnorderedResizingArrayPriorityQueue[Key] with MinPQ[Key]

  class MinPqWithOrderedResizingArray [Key : Ordering : ClassTag] extends OrderedResizingArrayPriorityQueue[Key] with MinPQ[Key]

  class MaxPqWithUnorderedLinkedList [Key : Ordering : ClassTag] extends UnorderedLinkedPriorityQueue[Key] with MaxPQ[Key]

  class MaxPqWithOrderedLinkedList [Key : Ordering : ClassTag] extends OrderedLinkedPriorityQueue[Key] with MaxPQ[Key]

  class MinPqWithUnorderedLinkedList [Key : Ordering : ClassTag] extends UnorderedLinkedPriorityQueue[Key] with MinPQ[Key]

  class MinPqWithOrderedLinkedList [Key : Ordering : ClassTag] extends OrderedLinkedPriorityQueue[Key] with MinPQ[Key]

  class MaxPqWithHeap [Key : Ordering : ClassTag] extends HeapPriorityQueue[Key] with MaxPQ[Key]

  class MinPqWithHeap [Key : Ordering : ClassTag] extends HeapPriorityQueue[Key] with MinPQ[Key]

  object Prio extends Enumeration {
    type Prio = Value
    val min, max = Value
  }

  def apply [Key : Ordering : ClassTag] (p: Prio.Value) = p match {
    case Prio.min => new MinPqWithHeap[Key]
    case Prio.max => new MaxPqWithHeap[Key]
  }

}