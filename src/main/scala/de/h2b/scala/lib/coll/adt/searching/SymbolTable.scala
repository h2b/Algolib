/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt.searching

import scala.reflect.ClassTag

object SymbolTable {

  def apply [Key : Ordering : ClassTag, Value : ClassTag] () =
    binarySearchTree[Key, Value]()

  def sequentialSearch [Key, Value] (): BasicSymbolTable[Key, Value] =
    SequentialSearchTable[Key, Value]()

  def binarySearch [Key : Ordering : ClassTag, Value : ClassTag] (): OrderedSymbolTable[Key, Value] =
    BinarySearchTable[Key, Value]()

  def binarySearchTree [Key : Ordering, Value] (): OrderedSymbolTable[Key, Value] =
    BinarySearchTree[Key, Value]()

}