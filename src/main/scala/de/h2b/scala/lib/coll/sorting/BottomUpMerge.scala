/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.sorting

import scala.math._

/**
 * Implements a bottom-up merge sort algorithm for sorting arrays in place
 * (actually using some extra space for "abstract in-place merge").
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see [[http://algs4.cs.princeton.edu/22mergesort Section 2.2]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
object BottomUpMerge extends Merge {

  def sort [E : Ordering] (a: Array[E]): Unit = {
    val aux = a.clone() //allocate space just once and avoid requiring a class tag
    //do lg N passes of pairwise merges
    val n = a.length
    var sz = 1 //subarray size
    while (sz<n) {
      var lo = 0 //subarray index
      while (lo<n-sz) {
        val mid = lo+sz-1
        val hi = min(mid+sz, n-1)
    	  merge(a, lo, mid, hi, aux)
        lo += sz+sz
      }
      sz += sz
    }
    assert(isSorted(a))
  }

  override def toString = "BottomUpMergeSort"

}