/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt.searching

import de.h2b.scala.lib.coll.adt.Queue

/**
 * This class implements an ordered symbol table of generic key/value pairs.
 *
 * This implementation uses a binary search tree.
 *
 * The ''put'', ''get'' and ''contains'' operations take linear time in the
 * worst case, but logarithmic time on the average. All order-based operations
 * take time proportional to the height of the tree in the worst case; this
 * applies to ''minimum'', ''maximum'' and ''select'' as well as ''floor'',
 * ''ceiling'' and the tow-argument ''size''. The one-argument ''size'' and
 * ''is-empty'' are constant in time.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      [[http://algs4.cs.princeton.edu]]
 * @see [[http://algs4.cs.princeton.edu/32bst Section 3.2]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
class BinarySearchTree [Key, Value] private
    (implicit protected val ord: Ordering[Key]) extends OrderedSymbolTable[Key, Value] {

  import BinarySearchTree._

  private var root: Node[Key, Value] = null

  def size: Int = size(root)

  private def size (node: Node[Key, Value]) = if (node==null) 0 else node.n

	def apply (key: Key): Value = get(root, key)

	private def get (node: Node[Key, Value], key: Key): Value = {
		//return value associated with key in the subtree rooted at node
    if (node==null) throw new NoSuchElementException(s"not found: $key")
    val cmp = ord.compare(key, node.key)
    if (cmp<0) get(node.left, key)
    else if (cmp>0) get(node.right, key)
    else node.value
  }

	def update (key: Key, value: Value): Unit = {
	  root = put(root, key, value)
	}

	private def put (node: Node[Key, Value], key: Key, value: Value): Node[Key, Value] = {
	  //search for key in subtree rooted at node and update value if found
	  //or else add new node to subtree
	  //returns the subtree with the updated or added node
	  if (node==null) return new Node(key, value, null, null, 1)
	  val cmp = ord.compare(key, node.key)
	  if (cmp<0) node.left = put(node.left, key, value)
	  else if (cmp>0) node.right = put(node.right, key, value)
	  else node.value = value
	  node.n = size(node.left)+size(node.right)+1
	  node
	}

  def min: Key = {
    if (root==null) throw new NoSuchElementException("tree is empty")
    min(root).key
  }

  private def min (node: Node[Key, Value]): Node[Key, Value] =
    if (node.left==null) node
    else min(node.left)

  def max: Key = {
    if (root==null) throw new NoSuchElementException("tree is empty")
    max(root).key
  }

  private def max (node: Node[Key, Value]): Node[Key, Value] =
    if (node.right==null) node
    else max(node.right)

  def floor (key: Key): Key = {
		val node = floor(root, key)
		if (node==null) throw new NoSuchElementException(s"less than minimum key: $key")
		node.key
  }

  private def floor (node: Node[Key, Value], key: Key): Node[Key, Value] = {
    if (node==null) return null
    val cmp = ord.compare(key, node.key)
    if (cmp==0) return node
    if (cmp<0) return floor(node.left, key)
    //cmp>0, evaluate right subtree
    val rFloor = floor(node.right, key)
    if (rFloor==null) node //no greater key in right subtree smaller than key
    else rFloor //node in right subtree with key greater than node.key but smaller than key
  }

  def ceil (key: Key): Key = {
		val node = ceil(root, key)
		if (node==null) throw new NoSuchElementException(s"greater than maximum key: $key")
		node.key
  }

  private def ceil (node: Node[Key, Value], key: Key): Node[Key, Value] = {
    if (node==null) return null
    val cmp = ord.compare(key, node.key)
    if (cmp==0) return node
    if (cmp>0) return ceil(node.right, key)
    //cmp<0, evaluate left subtree
    val lCeil = ceil(node.left, key)
    if (lCeil==null) node //no smaller key in left subtree greater than key
    else lCeil //node in left subtree with key smaller than node.key but greater than key
  }

  def select (k: Int): Key = {
    val node = select(root, k)
    if (node==null) throw new NoSuchElementException(s"rank out of bounds: $k")
    node.key
  }

  private def select (node: Node[Key, Value], k: Int): Node[Key, Value] = {
    if (node==null) return null
    val lSize = size(node.left)
    if (lSize>k) select(node.left, k)
    else if (lSize<k) select(node.right, k-lSize-1)
    else node
  }

  def rank (key: Key): Int = rank(root, key)

  private def rank (node: Node[Key, Value], key: Key): Int = {
    if (node==null) return 0
    val cmp = ord.compare(key, node.key)
    if (cmp<0) rank(node.left, key)
    else if (cmp>0) 1+size(node.left)+rank(node.right, key)
    else size(node.left)
  }

  override def deleteMin (): Unit = {
    if (root==null) throw new NoSuchElementException("tree is empty")
    root = deleteMin(root)
  }

  private def deleteMin (node: Node[Key, Value]): Node[Key, Value] = {
    if (node.left==null) return node.right
    node.left = deleteMin(node.left)
    node.n = size(node.left)+size(node.right)+1
    node
  }

  override def deleteMax (): Unit = {
    if (root==null) throw new NoSuchElementException("tree is empty")
    root = deleteMax(root)
  }

  private def deleteMax (node: Node[Key, Value]): Node[Key, Value] = {
    if (node.right==null) return node.left
    node.right = deleteMin(node.right)
    node.n = size(node.left)+size(node.right)+1
    node
  }

	def delete (key: Key): Unit = {
	  root = delete(root, key)
	}

	private def delete (node: Node[Key, Value], key: Key): Node[Key, Value] = {
	  if (node==null) throw new NoSuchElementException(s"not found: $key")
	  var current = node
	  val cmp = ord.compare(key, current.key)
	  if (cmp<0) current.left = delete(current.left, key)
	  else if (cmp>0) current.right = delete(current.right, key)
	  else {
	    if (current.right==null) return current.left
	    if (current.left==null) return current.right
	    //replace current by smallest node of right subtree (eager Hibbard deletion)
	    val delNode = current
	    current = min(delNode.right)
	    current.right = deleteMin(delNode.right)
	    current.left = delNode.left
	  }
	  current.n = size(current.left)+size(current.right)+1
	  current
	}

  def keys (lo: Key, hi: Key): Iterable[Key] = {
    val q = Queue[Key]()
    enqueue(q, root, lo, hi)
    q
  }

  private def enqueue (queue: Queue[Key], node: Node[Key, Value], lo: Key, hi: Key): Unit = {
    //add all keys from the subtree rooted at node in sorted order to queue
    if (node==null) return
    val cmplo = ord.compare(lo, node.key)
    val cmphi = ord.compare(node.key, hi)
    if (cmplo<0) enqueue(queue, node.left, lo, hi)
    if (cmplo<=0 && cmphi<=0) queue.enqueue(node.key)
    if (cmphi<0) enqueue(queue, node.right, lo, hi)
  }

	override def toString = "BinarySearchTree"

}

object BinarySearchTree {

  private class Node [Key, Value] (val key: Key, var value: Value,
      var left: Node[Key, Value], var right: Node[Key, Value], var n: Int)

  def apply [Key : Ordering, Value] () = new BinarySearchTree[Key, Value]()

}