/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt.searching

/**
 * This trait represents a symbol table of generic key/value pairs.
 *
 * It supports the usual ''put'', ''get'', ''contains'', ''delete'', ''size'',
 * and ''is-empty'' methods. It also provides a ''keys'' method for iterating
 * over all of the keys.
 *
 * A symbol table implements the ''associative array'' abstraction:
 * when associating a value with a key that is already in the symbol table,
 * the convention is to replace the old value with the new value.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      [[http://algs4.cs.princeton.edu]]
 * @see [[http://algs4.cs.princeton.edu/31elementary Section 3.1]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
trait BasicSymbolTable [Key, Value] {

  /**
   * Inserts key/value pair into table. Replaces value if key is already there.
   */
	def update (key: Key, value: Value): Unit

	/**
	 * @return value paired with key
	 * @throws NoSuchElementException if key is not in table
	 */
	def apply (key: Key): Value

	def get (key: Key): Option[Value] = try {
    return Some(apply(key))
  } catch {
    case _: NoSuchElementException => return None
  }

	def getOrElse (key: Key, default: => Value): Value = try {
    return apply(key)
  } catch {
    case _: NoSuchElementException => return default
  }

	/**
	 * Removes key/value pair from table.
	 *
	 * @throws NoSuchElementException if key is not in table
	 */
	def delete (key: Key): Unit

  def contains (key: Key): Boolean = get(key)!=None

  def isEmpty: Boolean = size==0

  def size: Int

  /**
   * @return all keys in the table
   */
  def keys (): Iterable[Key]

  override def toString = "SymbolTable"

}