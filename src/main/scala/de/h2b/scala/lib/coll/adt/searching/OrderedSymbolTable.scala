/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt.searching

/**
 * This trait represents an ordered symbol table of generic key/value pairs.
 *
 * It supports the usual ''put'', ''get'', ''contains'', ''delete'', ''size'',
 * and ''is-empty'' methods. It also provides a ''keys'' method for iterating
 * over all of the keys.
 *
 * It also provides ordered methods for finding the ''minimum'', ''maximum'',
 * ''floor'', and ''ceiling''.
 *
 * A symbol table implements the ''associative array'' abstraction:
 * when associating a value with a key that is already in the symbol table,
 * the convention is to replace the old value with the new value.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      [[http://algs4.cs.princeton.edu]]
 * @see [[http://algs4.cs.princeton.edu/31elementary Section 3.1]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
trait OrderedSymbolTable [Key, Value] extends BasicSymbolTable[Key, Value] {

  protected val ord: Ordering[Key]

  /**
   * @return the smallest key
	 * @throws NoSuchElementException if table is empty
   */
  def min: Key

  /**
   * @return the largest key
	 * @throws NoSuchElementException if table is empty
   */
  def max: Key

  /**
   * @return the largest key less than or equal to the specified key
	 * @throws NoSuchElementException if key is less than minimum key
   */
  def floor (key: Key): Key

  /**
   * @return the smallest key greater than or equal to the specified key
	 * @throws NoSuchElementException if key is greater than maximum key
   */
  def ceil (key: Key): Key

  /**
   * @return the number of keys less than the specified key
   */
  def rank (key: Key): Int

  /**
   * @return the key of rank k
	 * @throws NoSuchElementException if k is less than 0 or greater than the
	 * maximum rank
   */
  def select (k: Int): Key

  /**
   * Removes the node with the the smallest key.
	 * @throws NoSuchElementException if table is empty
   */
  def deleteMin (): Unit = delete(min)

  /**
   * Removes the node with the the largest key.
	 * @throws NoSuchElementException if table is empty
   */
  def deleteMax (): Unit = delete(max)

  override def size: Int

  /**
   * @return the number of keys between lo and hi (inclusive)
   */
  def size (lo: Key, hi: Key): Int =
    if (ord.compare(hi, lo)<0) 0
    else if (contains(hi)) rank(hi)-rank(lo)+1
    else rank(hi)-rank(lo)

  /**
   * @return all keys in the table between lo and hi (inclusive) in sorted order
   */
  def keys (lo: Key, hi: Key): Iterable[Key]

  override def keys (): Iterable[Key] = keys(min, max)

  override def toString = "OrderedSymbolTable"

}