/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt.searching

import scala.reflect.ClassTag
import de.h2b.scala.lib.coll.adt.Queue

/**
 * This class implements an ordered symbol table of generic key/value pairs.
 *
 * This implementation uses a resizing array and binary search.
 *
 * The ''get' and ''contains'' as well as the tow-argument ''size'', the
 * ''floor'' and the ''ceiling'' operations take logarithmic time. The
 * ''minimum'', ''maximum'' and ''select'' as well as ''is-empty'' and the
 * one-argument ''size'' operations are constant in time. The ''put'' operation
 * is linear in time in the worst case.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      [[http://algs4.cs.princeton.edu]]
 * @see [[http://algs4.cs.princeton.edu/31elementary Section 3.1]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
class BinarySearchTable [Key : ClassTag, Value : ClassTag] private
    (implicit protected val ord: Ordering[Key]) extends OrderedSymbolTable[Key, Value] {

  private var keyArray = Array.ofDim[Key](2)
  private var valArray = Array.ofDim[Value](2)
  private var n = 0 //number of key/value pairs

  private def resize (capacity: Int) = {
    assert(capacity>=n)
    val newKeyArray = Array.ofDim[Key](capacity)
    val newValArray = Array.ofDim[Value](capacity)
    for (i <- 0 until n) {
      newKeyArray(i) = keyArray(i)
      newValArray(i) = valArray(i)
    }
    keyArray = newKeyArray
    valArray = newValArray
  }

  def size: Int = n

	def apply (key: Key): Value = {
    val i = rank(key)
    if (i<n && ord.compare(keyArray(i), key)==0) valArray(i)
    else throw new NoSuchElementException(s"not found: $key")
  }

	def update (key: Key, value: Value): Unit = {
	  //search for key and update value if found or grow table else
	  val i = rank(key)
	  if (i<n && ord.compare(keyArray(i), key)==0) {
	    valArray(i) = value
	    return
	  }
	  if (n==keyArray.length) resize(n*2)
	  for (j <- n until i by -1) {
	    keyArray(j) = keyArray(j-1)
	    valArray(j) = valArray(j-1)
	  }
	  keyArray(i) = key
	  valArray(i) = value
	  n += 1
	}

	def delete (key: Key): Unit = {
	  //search for key and delete it
	  val i = rank(key)
	  if (i<n && ord.compare(keyArray(i), key)==0) {
	    delete(i)
	    return
	  }
    throw new NoSuchElementException(s"not found: $key")
	}

	private def delete (i: Int) = {
	  for (j <- i+1 until n) {
	    keyArray(j-1) = keyArray(j)
	    valArray(j-1) = valArray(j)
	  }
	  n -= 1
	  keyArray(n) = _: Key
	  valArray(n) = _: Value
    if (n>0 && n==keyArray.length/4) resize(keyArray.length/2)
	}

  def rank (key: Key): Int = {
    var lo = 0
    var hi = n-1
    while (lo<=hi) {
      val mid = lo+(hi-lo)/2
      val cmp = ord.compare(key, keyArray(mid))
      if (cmp<0) hi = mid-1
      else if (cmp>0) lo = mid+1
      else return mid
    }
    lo
  }

  def min: Key = {
    if (isEmpty) throw new NoSuchElementException("table empty")
    keyArray(0)
  }

  def max: Key = {
    if (isEmpty) throw new NoSuchElementException("table empty")
    keyArray(n-1)
  }

  def select (k: Int): Key = {
    if (k<0 || n<=k) throw new NoSuchElementException(s"rank out of bounds: $k")
    keyArray(k)
  }

  def ceil (key: Key): Key = {
    val i = rank(key)
    if (i>=n) throw new NoSuchElementException(s"greater than maximum key: $key")
    keyArray(i)
  }

  def floor (key: Key): Key = {
    val i = rank(key)
    if (i<n && ord.compare(key, keyArray(i))==0) return keyArray(i)
    if (i==0) throw new NoSuchElementException(s"less than minimum key: $key")
    keyArray(i-1)
  }

  def keys (lo: Key, hi: Key): Iterable[Key] = {
    val q = Queue[Key]()
    val rlo = rank(lo)
    val rhi = rank(hi)
    for (i <- rlo until rhi) q.enqueue(keyArray(i))
    if (rlo<=rhi && contains(hi)) q.enqueue(keyArray(rhi))
    q
  }

	override def toString = "BinarySearchTable"

}

object BinarySearchTable {

  def apply [Key : Ordering : ClassTag, Value : ClassTag] () = new BinarySearchTable[Key, Value]()

}