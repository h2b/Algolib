/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.sorting

/**
 * Implements a 2-way quick sort algorithm for sorting arrays in place.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see [[http://algs4.cs.princeton.edu/23quicksort Section 2.3]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
object Quick2Way extends Quick {

  protected def sort [E : Ordering] (a: Array[E], lo: Int, hi: Int): Unit = {
    if (hi<=lo) return
    val j = partition(a, lo, hi)
    sort(a, lo, j-1) //sort left part
    sort(a, j+1, hi) //sort right part
  }

  private def partition [E : Ordering] (a: Array[E], lo: Int, hi: Int) = {
	  //partition into a(lo..j-1), a(j), a(j+1..hi) and return j
    var i = lo //left scan index
    var j = hi+1 //right scan index
    val v = a(lo) //partitioning item
    while (i<j) {
      //scan right, scan left, check for scan complete and exchange
      do i += 1 while (i<hi && lt(a(i),v))
      do j -= 1 while (j>lo && lt(v,a(j)))
      if (i<j) swap(a, i, j)
    }
    swap(a, lo, j) //put partioning item into final position
    j //with a(lo..j-1) <= a(j) <= a(j+1..hi)
  }

  override def toString = "2WayQuickSort"

}