/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

import scala.reflect.ClassTag

/**
 * Provides a LIFO stack of generic items, supporting {@code push} and
 * {@code pop} operations, along with methods for peeking at the top item,
 * testing if the stack is empty, and iterating through the items in LIFO order.
 * <p>
 * This implementation uses a resizing array, which doubles the underlying array
 * when it is full and halves the underlying array when it is one-quarter full.
 * The {@code push} and {@code pop} operations take constant amortized time.
 * The {@code size}, {@code peek}, and {@code isEmpty} operations take
 * constant time in the worst case.
 * Iteration takes time proportional to the number of items.
 * <p>
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see <a href="http://algs4.cs.princeton.edu/13stacks">Section 1.3</a> of
 *      <i>Algorithms, 4th Edition</i>
 * @author h2b
 */
class ResizingArrayStack[A : ClassTag] private extends Stack[A] {

  private var s = Array.ofDim[A](2) //the stack

  private var n = 0 //number of items

	override def isEmpty: Boolean = n==0

  override def size: Int = n

  def peek: A = {
    if (isEmpty) throw new NoSuchElementException("Stack underflow")
    s(n-1)
  }

  def push (item: A) = {
    if (n==s.length) resize(s.length*2)
    s(n) = item
    n += 1
  }

  private def resize (capacity: Int) = {
    assert(capacity>=n)
    val ss = Array.ofDim[A](capacity)
    for (i <- 0 until n) ss(i) = s(i)
    s = ss
  }

  def pop (): A = {
    if (isEmpty) throw new NoSuchElementException("Stack underflow")
    val item = s(n-1)
    s(n-1) = _: A //to avoid loitering
    n -= 1
    if (n>0 && n==s.length/4) resize(s.length/2)
    item
  }

  def iterator = new Iterator[A] {
    private var i = n-1
    def hasNext = i>=0
    def next = {
      if (!hasNext) throw new NoSuchElementException
      val item = s(i)
      i -= 1
      item
    }
  }

}

object ResizingArrayStack {

  def apply [A : ClassTag] () = new ResizingArrayStack[A]

}