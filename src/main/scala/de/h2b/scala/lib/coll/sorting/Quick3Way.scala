/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.sorting

/**
 * Implements a 3-way quick sort algorithm for sorting arrays in place.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see [[http://algs4.cs.princeton.edu/23quicksort Section 2.3]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
object Quick3Way extends Quick {

  protected def sort [E] (a: Array[E], lo: Int, hi: Int)  (implicit ord: Ordering[E]): Unit = {
    if (hi<=lo) return
    var lt = lo
    var i = lo+1
    var gt = hi
    val v = a(lo)
    while (i<=gt) {
    	ord.compare(a(i), v) match {
    	case sgn if sgn<0 => { swap(a, lt, i); lt += 1; i += 1 }
    	case sgn if sgn>0 => { swap(a, i, gt); gt -= 1 }
    	case _ => i += 1
    	}
    }
    //now a(lo..lt-1) < v = a(lt..gt) < a(gt+1..hi)
    sort(a, lo, lt-1)
    sort(a, gt+1, hi)
  }

  override def toString = "3WayQuickSort"

}