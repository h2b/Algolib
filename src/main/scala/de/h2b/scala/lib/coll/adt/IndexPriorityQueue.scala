/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

import scala.reflect.ClassTag

/**
 * This abstract class represents an indexed priority queue of generic keys.
 * It supports the usual ''insert'' and ''delete'' operations, along with
 * methods for changing a given key, peeking at the highest-priority key,
 * testing if the priority queue is empty, and iterating through the keys.
 *
 * In order to let the client refer to items on the priority queue,
 * an integer between 0 and length-1 is associated with each key;the client
 * uses this integer to specify which key to delete or change.
 *
 * This implementation uses a binary heap along with additional arrays to
 * associate keys with integers in the given range.
 *
 * The ''insert'', ''delete'' and ''change-key'', operations take logarithmic
 * time. The ''is-empty'', ''size'', ''highest-index'', ''highest-key'' and
 * ''key-of'' operations take constant time. Construction takes time
 * proportional to the specified capacity.
 *
 * This abstract class  supports both minimum and maximum priority-key
 * implementations by means of the {@code ht} method.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see <a href="http://algs4.cs.princeton.edu/24pq">Section 2.4</a> of
 *      <i>Algorithms, 4th Edition</i>
 * @author h2b
 *
 * @param <Key> the type of the keys
 *
 * @constructor
 *
 * @param length the length of this priority queue
 */
abstract class IndexPriorityQueue [Key : Ordering : ClassTag] (val length: Int) extends Iterable[Key] {

  protected val key = Array.ofDim[Key](length+1) //the heap of keys
  protected val index = Array.ofDim[Int](length+1) //the associated indices
  private val revdex = Array.ofDim[Int](length+1) //the reverse indices: revdex(index(i))==index(revdex(i))==i

  private final val nondex = -1 //indicates that revdex(i) is not on queue for i

  for (i <- 0 to length) revdex(i) = nondex

	protected var n = 0 //numer of items

	/**
	 * Insert key, associated with index i.
	 *
	 * @param i
	 * @param item
   * @throws IndexOutOfBoundsException if not `0 <= i < length`
   * @throws IllegalArgumentException if i is already on queue
	 */
	def enqueue (i: Int, item: Key): Unit = {
    require(!contains(i), s"Index $i already in priority queue")
    //append new item at the end of the array and swim up
    n += 1
    revdex(i) = n
    index(n) = i
    key(i) = item
    swim(n)
  }

	/**
	 * Change the key associated with index i.
	 *
	 * @param i
	 * @param item
   * @throws NoSuchElementException if no key is associated with index i
   * @throws IndexOutOfBoundsException if not `0 <= i < length`
	 */
	def update (i: Int, item: Key): Unit = {
    if (!contains(i)) throw new NoSuchElementException(s"No such index: $i")
    key(i) = item
    swim(revdex(i))
    sink(revdex(i))
  }


	/**
	 * @param i
	 * @return if index i is associated with some key
   * @throws IndexOutOfBoundsException if not `0 <= i < length`
	 */
	def contains (i: Int): Boolean = {
    if (i<0 || length<=i) throw new IndexOutOfBoundsException(s"Index out of range: $i")
	  revdex(i)!=nondex
 }

	/**
	 * Remove the key associated with index i.
	 *
	 * @param i
   * @throws NoSuchElementException if no key is associated with index i
   * @throws IndexOutOfBoundsException if not `0 <= i < length`
	 */
	def delete (i: Int): Unit = {
    if (!contains(i)) throw new NoSuchElementException(s"No such index: $i")
    val k = revdex(i)
    swap(k, n)
    n -= 1
    swim(k)
    sink(k)
    key(i) = _: Key //to avoid loitering
    revdex(i) = nondex
  }


  /**
   * Returns the item with highest priority from this queue without consuming
   * it.
   *
   * @return the highest-priority item from this queue
   * @throws NoSuchElementException if this priority queue is empty
   */
  def peekKey: Key = {
    if (isEmpty) throw new NoSuchElementException("Priority queue underflow")
    key(index(1))
  }

  /**
   * Returns the index of the item with highest priority from this queue without
   * consuming it.
   *
   * @return the index of the highest-priority item from this queue
   * @throws NoSuchElementException if this priority queue is empty
   */
  def peekIndex: Int = {
    if (isEmpty) throw new NoSuchElementException("Priority queue underflow")
    index(1)
  }

  /**
   * Removes the item with highest priority from this queue and returns its
   * index.
   *
   * @return the index of the highest-priority item from this queue
   * @throws NoSuchElementException if this priority queue is empty
   */
  def dequeue (): Int = {
    if (isEmpty) throw new NoSuchElementException("Priority queue underflow")
    //exchange first item with the last, delete it and let new first item sink down
    val high = index(1)
    swap(1, n)
    key(index(n)) = _: Key //to avoid loitering
    revdex(index(n)) = nondex
    n -= 1
    sink(1)
    high
  }

  override def isEmpty: Boolean = size==0

  /**
   * @return the number of keys in this priority queue
   */
  override def size: Int = n

  /**
   * @param i
   * @return the key associated with index i
   * @throws NoSuchElementException if no key is associated with index i
   * @throws IndexOutOfBoundsException if not `0 <= i < length`
   */
  def apply (i: Int): Key = {
    if (!contains(i)) throw new NoSuchElementException(s"No such index: $i")
    key(i)
  }

  /**
   * Returns an iterator that iterates over the items in this queue in FIFO order
   * with (!) consuming them.
   * .
   * @return the iterator
   */
  def iterator = new Iterator[Key] {
    def hasNext = !IndexPriorityQueue.this.isEmpty //don't use Iterator's own isEmpty
    def next = {
      if (!hasNext) throw new NoSuchElementException
      val key = peekKey
      dequeue()
      key
    }
  }

  /**
   * Returns if the key associated with index i has higher priority than the key
   * associated with index j.
   *
   * @param i
   * @param j
   * @return key(index(i)) has higher priority than key(index(j))
   */
  protected def ht (i: Int, j: Int) (implicit ord: Ordering[Key]): Boolean

	/**
   * Exchanges indices i and j and its reverse counterparts.
   *
	 * @param i
	 * @param j
	 */
	private def swap (i: Int, j: Int) = {
    require(0<=i && i<length && 0<=j && j<length)
		val t = index(i)
		index(i) = index(j)
		index(j) = t
		revdex(index(i)) = i
		revdex(index(j)) = j
	}

	/**
	 * Bottom-up reheapify.
	 *
	 * @param k
	 */
	private def swim (k: Int) = {
	  var j = k
		while (j>1 && ht(j,j/2)) {
		  swap(j, j/2)
		  j /= 2
		}
  }

	/**
	 * Top-down reheapify.
	 *
	 * @param k
	 */
	private def sink (k: Int): Unit = {
		var j = k
		while (2*j<=n) {
		  var i = 2*j
		  if (i<n && ht(i+1,i)) i += 1
		  if (!ht(i,j)) return
		  swap(i, j)
		  j = i
		}
	}

  override def toString = "IndexPriorityQueue"

}

object IndexPriorityQueue {

  trait PriorityOrder

  trait MaxPQ [Key] extends IndexPriorityQueue[Key] with PriorityOrder {
    protected def ht (i: Int, j: Int) (implicit ord: Ordering[Key]): Boolean = ord.gt(key(index(i)),key(index(j)))
    override def toString = super.toString + "Max"
  }

  trait MinPQ [Key] extends IndexPriorityQueue[Key] with PriorityOrder {
    protected def ht (i: Int, j: Int) (implicit ord: Ordering[Key]): Boolean = ord.lt(key(index(i)),key(index(j)))
    override def toString = super.toString + "Min"
  }

	class IndexMaxPq [Key : Ordering : ClassTag] (length: Int) extends IndexPriorityQueue[Key](length) with MaxPQ[Key]

  class IndexMinPq [Key : Ordering : ClassTag] (length: Int) extends IndexPriorityQueue[Key](length) with MinPQ[Key]

  object Prio extends Enumeration {
    type Prio = Value
    val min, max = Value
  }

  def apply [Key : Ordering : ClassTag] (length: Int, p: Prio.Value) = p match {
    case Prio.min => new IndexMinPq[Key](length)
    case Prio.max => new IndexMaxPq[Key](length)
  }

}