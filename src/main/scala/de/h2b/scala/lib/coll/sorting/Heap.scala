/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.sorting

/**
 * Implements a heap sort algorithm for sorting arrays in place.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see [[http://algs4.cs.princeton.edu/24pq Section 2.4]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
object Heap extends Sort {

  /*
   * Implemenation note: The heap will be constructed using indices 1..n while
   * the actual array indices are 0..n-1. This is solved by decrementing the
   * indices in the swap and ht methods.
   */

	def sort [E : Ordering] (a: Array[E]): Unit = {
		var n = a.length
		//heap construction
		for (k <- n/2 to 1 by -1) sink(a, k, n)
		//sortdown
		while (n>1) {
		  //put largest element to the end and reheapify the rest
		  swap(a, 1, n)
		  n -= 1
		  sink(a, 1, n)
		}
		assert(isSorted(a))
	}

	//top-down reheapify as in HeapPriorityQueue
	private def sink [E : Ordering] (a: Array[E], k: Int, n: Int): Unit = {
		var j = k
		while (2*j<=n) {
		  var i = 2*j
		  if (i<n && ht(a,i+1,i)) i += 1
		  if (!ht(a,i,j)) return
		  swap(a, i, j)
		  j = i
		}
	}

	//indices are decremented according to the implementation note above
	override protected def swap [E] (a: Array[E], i: Int, j: Int) = super.swap(a, i-1, j-1)

	//heap construction has to build a maximum priority queue as in PriorityQueue.MaxPQ;
	//indices are decremented according to the implementation note above
	private def ht [E] (a: Array[E], i: Int, j: Int) (implicit ord: Ordering[E]): Boolean = ord.gt(a(i-1), a(j-1))

  override def toString = "HeapSort"

}