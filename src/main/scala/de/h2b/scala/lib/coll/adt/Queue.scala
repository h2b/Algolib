/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

import scala.collection.Iterable

/**
 * Provides a FIFO queue of generic items, supporting {@code enqueue} and
 * {@code dequeue} operations, along with methods for peeking at the first item,
 * testing if the queue is empty, and iterating through the items in FIFO order.
 * <p>
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see <a href="http://algs4.cs.princeton.edu/13stacks">Section 1.3</a> of
 *      <i>Algorithms, 4th Edition</i>
 * @author h2b
 */
trait Queue[A] extends Iterable[A] {

	def isEmpty: Boolean

  def size: Int

  /**
   * Returns the item least recently added to this queue without consuming it.
   *
   * @return first item of this queue
   * @throws NoSuchElementException if this queue is empty
   */
  def peek: A

  def enqueue (item: A)

  /**
   * Removes and returns the item on this queue that was least recently added.
   *
   * @return first item of this queue
   * @throws NoSuchElementException if this queue is empty
   */
  def dequeue (): A

  /**
   * Returns a string representation of this queue without consuming it.
   *
   * @return the sequence of items in FIFO order
   */
  override def toString = mkString("(", ", ", ")")

  /**
   * Returns an iterator that iterates over the items in this queue in FIFO order
   * without consuming them.
   * .
   * @return the iterator
   */
  def iterator: Iterator[A]

}

object Queue {

  def apply [A] () = LinkedQueue[A]()

}