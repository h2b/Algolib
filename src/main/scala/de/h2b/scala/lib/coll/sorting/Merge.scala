/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.sorting

/**
 * Defines an intermediate layer for implementation by various objects for
 * sorting arrays in place (but using some extra space, i.e., "abstract in-place
 * merge") using the mergesort algorithms.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see [[http://algs4.cs.princeton.edu/22mergesort Section 2.2]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
trait Merge extends Sort {

	/**
   * Merges a,,lo..mid,, with a,,mid+1..hi,,.
   *
	 * @param a array to be sorted
	 * @param lo lower index
	 * @param mid middle index
	 * @param hi higher index
	 * @param aux auxiliary array of same type and size as a
	 * @param evidence$1
   * @throws IllegalArgumentException if not {@code 0<=lo<=mid<=hi<a.length} and
   *         {@code a.length==aux.length}
   * @usecase def merge [E] (a: Array[E], lo: Int, mid: Int, hi: Int, aux: Array[E])
	 */
	protected def merge [E : Ordering] (a: Array[E], lo: Int, mid: Int, hi: Int, aux: Array[E]): Unit = {
		require(0<=lo && lo<=mid && mid<=hi && hi<a.length)
    require(a.length==aux.length)
		var i = lo
		var j = mid+1
		for (k <- lo to hi) {
			//copy a(lo..hi) to aux(lo..hi)
			aux(k) = a(k)
		}
		for (k <- lo to hi) {
			//merge back to a(lo..hi)
			if (i>mid) { a(k) = aux(j); j += 1 }
			else if (j>hi) { a(k) = aux(i); i += 1 }
			else if (lt(aux(j),aux(i))) { a(k) = aux(j); j += 1 }
			else { a(k) = aux(i); i += 1 }
		}
  }

}

object Merge {

  def apply (): Merge = TopDownMerge

}