/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

import scala.reflect.ClassTag

/**
 * Provides a bag (or multiset) of generic items, supporting insertion and
 * iterating over the items in arbitrary order.
 * <p>
 * This implementation uses a resizing array.
 * The {@code add} operation takes constant amortized time; the
 * {@code isEmpty}, and {@code size} operations
 * take constant time. Iteration takes time proportional to the number of items.
 * <p>
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see <a href="http://algs4.cs.princeton.edu/13stacks">Section 1.3</a> of
 *      <i>Algorithms, 4th Edition</i>
 * @author h2b
 */
class ResizingArrayBag[A : ClassTag] private extends Bag[A] {

  private var n = 0 //number of items

  private var a = Array.ofDim[A](2)

	override def isEmpty: Boolean = n==0

  override def size: Int = n

  def add (item: A) = {
    if (n==a.length) resize(a.length*2)
    a(n) = item
    n += 1
  }

  private def resize (capacity: Int) = {
    assert(capacity>=n)
    val aa = Array.ofDim[A](capacity)
    for (i <- 0 until n) aa(i) = a(i)
    a = aa
  }

  def iterator = new Iterator[A] {
    private var i = 0
    def hasNext = i<n
    def next = {
      if (!hasNext) throw new NoSuchElementException
      val item = a(i)
      i += 1
      item
    }
  }

}

object ResizingArrayBag {

  def apply [A : ClassTag] () = new ResizingArrayBag[A]

}