/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.sorting

/**
 * Implements selection sort for sorting arrays in place.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see [[http://algs4.cs.princeton.edu/21elementary Section 2.1]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
object Selection extends Sort {

  def sort [E : Ordering] (a: Array[E]): Unit = {
    val n = a.length
    for (i <- 0 until n) {
      //exchange a(i) with the smallest element in a(i+1..n)
      var min = i //index of the minimum element so far
      for (j <- i+1 until n) if (lt(a(j),a(min))) min = j
      swap(a, i, min)
    }
    assert(isSorted(a))
  }

  override def toString = "SelectionSort"

}