/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

/**
 * Provides a FIFO queue of generic items, supporting {@code enqueue} and
 * {@code dequeue} operations, along with methods for peeking at the first item,
 * <p>
 * This implementation uses a singly-linked list.
 * The {@code enqueue}, {@code dequeue}, {@code peek}, {@code size}, and
 * {@code isEmpty} operations all take constant time in the worst case.
 * Iteration takes time proportional to the number of items.
 * <p>
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see <a href="http://algs4.cs.princeton.edu/13stacks">Section 1.3</a> of
 *      <i>Algorithms, 4th Edition</i>
 * @author h2b
 */
class LinkedQueue[A] private extends Queue[A] {

  import LinkedQueue._

  private var n = 0 //number of items

  private var begin: Node[A] = null
  private var end: Node[A] = null

	override def isEmpty: Boolean = begin==null

  override def size: Int = n

  def peek: A = {
    if (isEmpty) throw new NoSuchElementException("Queue underflow")
    begin.item
  }

  def enqueue (item: A) = {
    val oldend = end
    end = new Node(item, null)
    if (isEmpty) begin = end else oldend.next = end
    n += 1
  }

  def dequeue (): A = {
    if (isEmpty) throw new NoSuchElementException("Queue underflow")
    val item = begin.item
    begin = begin.next
    n -= 1
    if (isEmpty) end = null //to avoid loitering
    item
  }


  def iterator = new Iterator[A] {
    private var current = begin
    def hasNext = current!=null
    def next = {
      if (!hasNext) throw new NoSuchElementException
      val item = current.item
      current = current.next
      item
    }
  }

}

object LinkedQueue{

  private class Node[A] (val item: A, var next: Node[A])

  def apply [A] () = new LinkedQueue[A]

}