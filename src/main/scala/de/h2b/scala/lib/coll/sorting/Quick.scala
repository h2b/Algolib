/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.sorting

import de.h2b.scala.lib.math.stat.Shuffle

/**
 * Defines an intermediate layer to be implemented by various objects for
 * sorting arrays in place using the quicksort algorithms.
 *
 * @author h2b
 *
 */
trait Quick extends Sort {

  def sort [E : Ordering] (a: Array[E]): Unit = {
    Shuffle(a) //eliminate dependencies on input
    sort(a, 0, a.length-1)
    assert(isSorted(a))
  }

  protected def sort [E : Ordering] (a: Array[E], lo: Int, hi: Int): Unit

}

object Quick {

  def apply (): Quick = Quick3Way

}