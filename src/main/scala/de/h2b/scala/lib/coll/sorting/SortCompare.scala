/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.sorting

import scala.collection.mutable

import de.h2b.scala.lib.util.Timer
import de.h2b.scala.lib.math.stat.Uniform

/**
 * Provides methods to compare the running times of two or more sorting
 * algorithms.
 *
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see [[http://algs4.cs.princeton.edu/21elementary Section 2.1]] of
 *      ''Algorithms, 4th Edition''
 * @author h2b
 */
object SortCompare {

  private val uniform = Uniform()

	/**
   * Measures the running time of sorting a given array with a specified
   * algorithm.
   *
	 * @param alg the algorithm to be used
	 * @param a the array to be sorted
	 * @param evidence$2 implict ordering value
	 * @return the running time in milliseconds
	 */
	def time [E : Ordering] (alg: Sort, a: Array[E]): Long = {
	  val timer = Timer()
    alg.sort(a)
    val t = timer.stop()
	  println(alg + " time=" + t/1000.0 + "s")
	  t
  }

	/**
   * Measures the total running time for using an algorithm to sort t random
   * arrays of length n.
   *
	 * @param alg the algorithm to be used
	 * @param n the length of each test array (must be nonnegative)
	 * @param t the number of repetitions (must be nonnegative)
	 * @return the total running time in milliseconds
   * @throws IllegalArgumentException if n or t is negative
	 */
	def timeRandomInput (alg: Sort, n: Int, t: Int): Long = {
	  require(n>=0, s"negative array size: $n")
	  require(t>=0, s"negative repetition count: $t")
	  var total = 0L
	  val a = Array.ofDim[Double](n)
	  for (k <- 1 to t) {
		  //perform one experiment generating and sorting an array
		  for (i <- 0 until n) a(i) = uniform.next()
				  total += time(alg, a)
	  }
	  println(alg + " total=" + total/1000.0 + "s")
	  total
  }

  /**
   * Maesures the total running times of a sequence of algorithms when sorting t
   * random arrays of length n.
   *
   * @param algs the algorithms to be used
   * @param n the length of each test array (must be nonnegative)
   * @param t the number of repetitions (must be nonnegative)
   * @return a map of the total running times of each algorithm in milliseconds
   * @throws IllegalArgumentException if n or t is negative
   */
  def comparison (algs: Seq[Sort], n: Int, t: Int): Map[Sort, Long] = {
	  require(n>=0, s"negative array size: $n")
	  require(t>=0, s"negative repetition count: $t")
    val totals = mutable.Map.empty[Sort, Long]
    for (alg <- algs) totals(alg) = timeRandomInput(alg, n, t)
    totals.toMap
  }

}

object SortCompareDemo extends App {

  val n = 250000
  val t = 10

  val algorithms =
    Seq(Selection, Insertion, Shell, TopDownMerge, BottomUpMerge, Quick2Way, Quick3Way, Heap)

  val totals = SortCompare.comparison(algorithms, n, t)
  val sortedTotals = totals.toList.sortBy(_._2)
  val minTotal = sortedTotals.head._2

  println("           algorithm \t time/ms \t factor\n")
  for (t <- sortedTotals) {
    printf("%20s \t %7.3f \t %6d\n", t._1, t._2/1000.0, t._2/minTotal)
  }

}

/*
 * Sample output for n=250000 and t=10
 *
           algorithm 	 time/ms 	 factor

       2WayQuickSort 	   0,510 	      1
            HeapSort 	   0,514 	      1
       3WayQuickSort 	   0,603 	      1
           ShellSort 	   0,846 	      1
   BottomUpMergeSort 	   1,287 	      2
    TopDownMergeSort 	   1,378 	      2
       InsertionSort 	 150,623 	    295
       SelectionSort 	 284,437 	    557

 * Sample output for n=100000 and t=10
 *
           algorithm 	 time/ms 	 factor

       2WayQuickSort 	   0,204 	      1
            HeapSort 	   0,209 	      1
       3WayQuickSort 	   0,257 	      1
           ShellSort 	   0,282 	      1
   BottomUpMergeSort 	   0,477 	      2
    TopDownMergeSort 	   0,547 	      2
       InsertionSort 	  23,697 	    116
       SelectionSort 	  49,762 	    243

 * Sample output for n=10000 and t=10
 *
           algorithm 	 time/ms 	 factor

           ShellSort 	   0,030 	      1
            HeapSort 	   0,035 	      1
       2WayQuickSort 	   0,037 	      1
       3WayQuickSort 	   0,049 	      1
   BottomUpMergeSort 	   0,063 	      2
    TopDownMergeSort 	   0,077 	      2
       InsertionSort 	   0,267 	      8
       SelectionSort 	   0,664 	     22

 * Sample output for n=1000 and t=10
 *
           algorithm 	 time/ms 	 factor

           ShellSort 	   0,010 	      1
            HeapSort 	   0,015 	      1
       3WayQuickSort 	   0,015 	      1
       2WayQuickSort 	   0,020 	      2
    TopDownMergeSort 	   0,027 	      2
       InsertionSort 	   0,032 	      3
   BottomUpMergeSort 	   0,049 	      4
       SelectionSort 	   0,063 	      6

 *
 */
