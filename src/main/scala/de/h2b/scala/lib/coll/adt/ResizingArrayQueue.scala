/*
  Algolib - A Scala Library of Essential Algorithms

  Copyright 2015-2016 Hans-Hermann Bode

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

import scala.reflect.ClassTag

/**
 * Provides a FIFO queue of generic items, supporting {@code enqueue} and
 * {@code dequeue} operations, along with methods for peeking at the first item,
 * <p>
 * This implementation uses a resizing array, which doubles the underlying array
 * when it is full and halves the underlying array when it is one-quarter full.
 * The {@code enqueue} and {@code dequeue} operations take constant amortized time.
 * The {@code size}, {@code peek}, and {@code isEmpty} operations take
 * constant time in the worst case.
 * Iteration takes time proportional to the number of items.
 * <p>
 * Adapted from the book Algorithms 4 by R. Sedgewick and K. Wayne.
 *
 * @see Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 * @see <a href="http://algs4.cs.princeton.edu/13stacks">Section 1.3</a> of
 *      <i>Algorithms, 4th Edition</i>
 * @author h2b
 */
class ResizingArrayQueue[A : ClassTag] private extends Queue[A] {

  private var q = Array.ofDim[A](2) //the queue (wrap-around usage)

  private var begin = 0 //index of first element of queue
  private var end = 0 //index of next available slot
  private var n = 0 //number of items

	override def isEmpty: Boolean = n==0

  override def size: Int = n

  def peek: A = {
    if (isEmpty) throw new NoSuchElementException("Queue underflow")
    q(begin)
  }

  def enqueue (item: A) = {
    if (n==q.length) resize(q.length*2)
    q(end) = item
    end += 1
    if (end==q.length) end = 0 //wrap-around
    n += 1
  }

  private def resize (capacity: Int) = {
    assert(capacity>=n)
    val qq = Array.ofDim[A](capacity)
    for (i <- 0 until n) qq(i) = q((begin+i)%q.length)
    q = qq
    begin = 0
    end = n
  }

  def dequeue (): A = {
    if (isEmpty) throw new NoSuchElementException("Queue underflow")
    val item = q(begin)
    begin += 1
    if (begin==q.length) begin = 0 //wrap-around
    n -= 1
    if (n>0 && n==q.length/4) resize(q.length/2)
    item
  }

  def iterator = new Iterator[A] {
    private var i = 0
    def hasNext = i<n
    def next = {
      if (!hasNext) throw new NoSuchElementException
      val item = q((begin+i)%q.length)
      i += 1
      item
    }
  }

}

object ResizingArrayQueue {

  def apply [A : ClassTag] () = new ResizingArrayQueue[A]

}