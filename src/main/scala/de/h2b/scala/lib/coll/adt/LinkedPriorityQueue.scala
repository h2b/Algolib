/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

trait LinkedPriorityQueue [Key] extends PriorityQueue[Key] {

  import LinkedPriorityQueue._

  protected var top: Node[Key] = null

  protected var n = 0 //numer of items

  override def size: Int = n

	override def toString = super.toString + "Linked"

}

object LinkedPriorityQueue {

  protected[adt] class Node [A] (var item: A, var next: Node[A])

}

abstract class UnorderedLinkedPriorityQueue [Key : Ordering] extends LinkedPriorityQueue[Key] {

  import LinkedPriorityQueue._

  def enqueue (item: Key): Unit = {
    top = new Node(item, top)
    n += 1
  }

  def peek: Key = {
    if (isEmpty) throw new NoSuchElementException("Priority queue underflow")
    highestNode.item
  }

  private def highestNode = {
    require(top!=null)
    var result = top
    var node = top.next
    while (node!=null) {
      if (ht(node.item,result.item)) result = node
      node = node.next
    }
    result
  }

  def dequeue (): Key = {
    if (isEmpty) throw new NoSuchElementException("Priority queue underflow")
    swapItems(highestNode, top)
    val item = top.item
    top = top.next
    n -= 1
    item
  }

	private def swapItems (a: Node[Key], b: Node[Key]) = {
    require(a!=null && b!=null)
		val t = a.item
		a.item = b.item
		b.item = t
	}

	override def toString = super.toString + "Unordered"

}

abstract class OrderedLinkedPriorityQueue [Key : Ordering] extends LinkedPriorityQueue[Key] {

  import LinkedPriorityQueue._

	def enqueue (item: Key): Unit = {
    if (isEmpty || ht(item,top.item)) {
      top = new Node(item, top)
    } else {
    	var node = top
			while (node.next!=null && ht(node.next.item,item)) node = node.next
			node.next = new Node(item, node.next)
    }
    n += 1
  }

	def peek: Key = {
    if (isEmpty) throw new NoSuchElementException("Priority queue underflow")
    top.item
  }

	def dequeue (): Key = {
    if (isEmpty) throw new NoSuchElementException("Priority queue underflow")
    val item = top.item
    top = top.next
    n -= 1
    item
  }

	override def toString = super.toString + "Ordered"

}
