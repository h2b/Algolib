/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

import org.junit.runner.RunWith
import org.scalatest.{ BeforeAndAfter, FunSuite }
import org.scalatest.junit.JUnitRunner

/**
 * @author h2b
 */
@RunWith(classOf[JUnitRunner])
class ResizingArrayQueueTest extends FunSuite with BeforeAndAfter {

  var queue: ResizingArrayQueue[Int] = _

  before {
	  queue = ResizingArrayQueue[Int]
  }

  test("new queue is empty" ) {
    assert(queue.isEmpty)
  }

  test("new queue has size 0") {
    assert(0===queue.size)
  }

  test("enqueue some items and verify") {
    for (i <- 1 to 17) queue.enqueue(i)
    assert(!queue.isEmpty)
    assertResult(17)(queue.size)
    var items = Set(1 to 17: _*)
    for (item <- queue) items -= item
    assert(items.isEmpty)
  }

  test("dequeue items") {
    for (i <- 1 to 17) queue.enqueue(i)
    for (i <- 1 to 17) assertResult(i)(queue.dequeue())
    assert(queue.isEmpty)
  }

  test("peek item") {
    for (i <- 1 to 17) queue.enqueue(i)
    assertResult(1)(queue.peek)
    assertResult(17)(queue.size)
  }

}