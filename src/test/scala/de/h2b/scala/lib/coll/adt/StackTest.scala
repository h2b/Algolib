/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

import org.junit.runner.RunWith
import org.scalatest.{ BeforeAndAfter, FunSuite }
import org.scalatest.junit.JUnitRunner

/**
 * @author h2b
 */
@RunWith(classOf[JUnitRunner])
class StackTest extends FunSuite with BeforeAndAfter {

  var stack: Stack[Int] = _

  before {
	  stack = Stack[Int]
  }

  test("new stack is empty" ) {
    assert(stack.isEmpty)
  }

  test("new stack has size 0") {
    assert(0===stack.size)
  }

  test("push some items and verify") {
    stack.push(1)
    stack.push(2)
    stack.push(3)
    assert(!stack.isEmpty)
    assertResult(3)(stack.size)
    var items = Set(1,2,3)
    for (item <- stack) items -= item
    assert(items.isEmpty)
  }

  test("pop items") {
    stack.push(1)
    stack.push(2)
    stack.push(3)
    for (i <- 3 to 1 by -1) assertResult(i)(stack.pop())
    assert(stack.isEmpty)
  }

  test("peek item") {
    stack.push(1)
    stack.push(2)
    assertResult(2)(stack.peek)
    assertResult(2)(stack.size)
  }

  test("toString") {
    stack.push(1)
    stack.push(2)
    stack.push(3)
    assertResult("(3, 2, 1)")(stack.toString)
  }

}