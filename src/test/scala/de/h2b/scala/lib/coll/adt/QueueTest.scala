/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

import org.junit.runner.RunWith
import org.scalatest.{ BeforeAndAfter, FunSuite }
import org.scalatest.junit.JUnitRunner

/**
 * @author h2b
 */
@RunWith(classOf[JUnitRunner])
class QueueTest extends FunSuite with BeforeAndAfter {

  var queue: Queue[Int] = _

  before {
	  queue = Queue[Int]
  }

  test("new queue is empty" ) {
    assert(queue.isEmpty)
  }

  test("new queue has size 0") {
    assert(0===queue.size)
  }

  test("enqueue some items and verify") {
    queue.enqueue(1)
    queue.enqueue(2)
    queue.enqueue(3)
    assert(!queue.isEmpty)
    assertResult(3)(queue.size)
    var items = Set(1,2,3)
    for (item <- queue) items -= item
    assert(items.isEmpty)
  }

  test("dequeue items") {
    queue.enqueue(1)
    queue.enqueue(2)
    queue.enqueue(3)
    for (i <- 1 to 3) assertResult(i)(queue.dequeue())
    assert(queue.isEmpty)
  }

  test("peek item") {
    queue.enqueue(1)
    queue.enqueue(2)
    assertResult(1)(queue.peek)
    assertResult(2)(queue.size)
  }

  test("toString") {
    queue.enqueue(1)
    queue.enqueue(2)
    queue.enqueue(3)
    assertResult("(1, 2, 3)")(queue.toString)
  }

}