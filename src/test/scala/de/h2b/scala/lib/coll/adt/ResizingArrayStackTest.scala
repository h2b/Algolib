/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

import org.junit.runner.RunWith
import org.scalatest.{ BeforeAndAfter, FunSuite }
import org.scalatest.junit.JUnitRunner

/**
 * @author h2b
 */
@RunWith(classOf[JUnitRunner])
class ResizingArrayStackTest extends FunSuite with BeforeAndAfter {

  var stack: ResizingArrayStack[Int] = _

  before {
	  stack = ResizingArrayStack[Int]
  }

  test("new stack is empty" ) {
    assert(stack.isEmpty)
  }

  test("new stack has size 0") {
    assert(0===stack.size)
  }

  test("enstack some items and verify") {
    for (i <- 1 to 17) stack.push(i)
    assert(!stack.isEmpty)
    assertResult(17)(stack.size)
    var items = Set(1 to 17: _*)
    for (item <- stack) items -= item
    assert(items.isEmpty)
  }

  test("destack items") {
    for (i <- 1 to 17) stack.push(i)
    for (i <- 17 to 1 by -1) assertResult(i)(stack.pop())
    assert(stack.isEmpty)
  }

  test("peek item") {
    for (i <- 1 to 17) stack.push(i)
    assertResult(17)(stack.peek)
    assertResult(17)(stack.size)
  }

}