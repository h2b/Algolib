/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.sorting

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite

/**
 * @author h2b
 */
@RunWith(classOf[JUnitRunner])
class SortTest extends FunSuite {

  val algorithms: Seq[Sort] =
    Seq(Selection, Insertion, Shell, TopDownMerge, BottomUpMerge, Quick2Way, Quick3Way, Heap)

  for (alg <- algorithms) {

    test(s"$alg detects sorted array") {
      val a = Array(1, 2, 3)
      assert(alg.isSorted(a))
    }

    test(s"$alg detects unsorted array") {
      val a = Array(1, 3, 2)
      assert(!alg.isSorted(a))
    }

    test(s"$alg sorts sorted array") {
      val a = Array("a", "b", "c", "d")
      val expected = Array("a", "b", "c", "d")
      alg.sort(a)
      assertResult(expected)(a)
    }

    test(s"$alg sorts unsorted array") {
      val a = Array(3, 8, 1, 5, 7, 4, 9, 7, 1, 0, 6)
      val expected = Array(0, 1, 1, 3, 4, 5, 6, 7, 7, 8, 9)
      alg.sort(a)
      assertResult(expected)(a)
    }

  }

}