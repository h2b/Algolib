/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt.searching

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite

@RunWith(classOf[JUnitRunner])
class SymbolTableTest extends FunSuite {

  private val basicST: Seq[BasicSymbolTable[String, Int]] = Seq(
      SymbolTable.sequentialSearch[String, Int])

  private val orderedST: Seq[OrderedSymbolTable[String, Int]] = Seq(
      SymbolTable.binarySearch[String, Int],
      SymbolTable.binarySearchTree[String, Int])

  private val allST = basicST++orderedST

  private val keys = "SEARCHEXAMPLE".map(_.toString())

  for (st <- allST) {

    test(s"$st: new ST is empty") {
    	assert(st.isEmpty)
    }

    test(s"$st: new ST has size 0") {
      assert(st.size==0)
    }

    test(s"$st: applying for unknown key throws exception") {
      val unknownKey = "unknown key"
      assume(!st.contains(unknownKey))
      intercept[NoSuchElementException] {
        st(unknownKey)
      }
    }

    test(s"$st: deleting unknown key throws exception") {
      val unknownKey = "unknown key"
      assume(!st.contains(unknownKey))
      intercept[NoSuchElementException] {
        st.delete(unknownKey)
      }
    }

	  test(s"$st: putting in key/value pairs can be verified") {
		  for ((k,v) <- keys.zipWithIndex) st(k)=v
    	assertResult(Set(keys: _*).size)(st.size)
    	for (k <- keys) assert(st.contains(k))
    	assertResult(0)(st("S"))
    	assertResult(12)(st("E"))
    	assertResult(Some(8))(st.get("A"))
    	assertResult(3)(st.getOrElse("R", 0))
    }

	  test(s"$st: iterating over keys is possible") {
	    var set = Set(keys: _*)
	    for (k <- st.keys()) set -= k
	    assert(set.isEmpty)
	  }

  }

  for (st <- basicST) {

    test(s"$st: delete some key") {
      assume(st.contains("X"))
      val size = st.size
      st.delete("X")
      assertResult(None)(st.get("X"))
      assertResult(size-1)(st.size)
    }

  }

  for (st <- orderedST) {

    test(s"$st: minimum and maximum key") {
      assertResult("A")(st.min)
      assertResult("X")(st.max)
    }

    test(s"$st: floor and ceil") {
      assertResult("A")(st.floor("A"))
      assertResult("M")(st.floor("N"))
      assertResult("X")(st.floor("Z"))
      assertResult("X")(st.ceil("X"))
      assertResult("P")(st.ceil("N"))
      assertResult("A")(st.ceil("9"))
      intercept[NoSuchElementException](st.floor("9"))
      intercept[NoSuchElementException](st.ceil("Z"))
    }

    test(s"$st: rank") {
      assertResult(0)(st.rank("9"))
      assertResult(0)(st.rank("A"))
      assertResult(1)(st.rank("B"))
      assertResult(1)(st.rank("C"))
      assertResult(5)(st.rank("M"))
      assertResult(6)(st.rank("N"))
      assertResult(9)(st.rank("X"))
      assertResult(10)(st.rank("Z"))
    }

    test(s"$st: select") {
      for (key <- st.keys()) assert(st.select(st.rank(key))===key)
      intercept[NoSuchElementException](st.select(st.rank(st.max)+1))
    }

    test(s"$st: size(lo,hi)") {
      assertResult(st.size)(st.size(st.min, st.max))
      assertResult(0)(st.size("8", "9"))
      assertResult(1)(st.size("8", "A"))
      assertResult(3)(st.size("L", "P"))
      assertResult(3)(st.size("K", "Q"))
      assertResult(1)(st.size("X", "Z"))
      assertResult(0)(st.size("Y", "Z"))
      assertResult(0)(st.size("H", "C"))
    }

    test(s"$st: keys(lo,hi)") {
    	assertKeys(Set(keys: _*), st.keys(st.min, st.max))
    	assertKeys(Set.empty[String], st.keys("8", "9"))
    	assertKeys(Set("A"), st.keys("8", "A"))
    	assertKeys(Set("L","M","P"), st.keys("L", "P"))
    	assertKeys(Set("L","M","P"), st.keys("K", "Q"))
    	assertKeys(Set("X"), st.keys("X", "Z"))
    	assertKeys(Set.empty[String], st.keys("Y", "Z"))
    	assertKeys(Set.empty[String], st.keys("H", "C"))
    }

    def assertKeys (expected: Set[String], keys: Iterable[String]) = {
    	var actual = Set.empty[String]
    	for (key <- keys) actual += key
    	assertResult(expected)(actual)
    }

    test(s"$st: delete some key") {
      assume(st.contains("X"))
      val size = st.size
      st.delete("X")
      assertResult(None)(st.get("X"))
      assertResult(size-1)(st.size)
    }

    test(s"$st: delete min and max") {
    	for ((k,v) <- keys.zipWithIndex) st(k)=v
    	assume("A"==st.min)
    	assume("X"==st.max)
    	st.deleteMin()
      assertResult("C")(st.min)
      st.deleteMax()
      assertResult("S")(st.max)
    }

  }

}