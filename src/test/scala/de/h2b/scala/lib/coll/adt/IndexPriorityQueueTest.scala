/*
  Algolib - A Scala Library of Essential Algorithms
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.h2b.scala.lib.coll.adt

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import de.h2b.scala.lib.coll.adt.IndexPriorityQueue.Prio

@RunWith(classOf[JUnitRunner])
class IndexPriorityQueueTest extends FunSuite {

  private val maxLen = 10

  private val maxPqImpl = Seq(IndexPriorityQueue[Int](maxLen, Prio.max))
  private val minPqImpl = Seq(IndexPriorityQueue[Int](maxLen, Prio.min))

  private val allImpl = maxPqImpl++minPqImpl

  private val odds = Seq(1, 3, 5)
  private val evens = Seq(4, 2, 0)

  for (impl ← allImpl) {

	  var queue: IndexPriorityQueue[Int] = impl

    test(s"$impl: new queue is empty") {
      assert(queue.isEmpty)
    }

    test(s"$impl: new queue has size 0") {
      assert(0 === queue.size)
    }

    test(s"$impl: peeking index from empty queue throws exception") {
      assert(queue.isEmpty)
      intercept[NoSuchElementException] {
        queue.peekIndex
      }
    }

    test(s"$impl: peeking key from empty queue throws exception") {
      assert(queue.isEmpty)
      intercept[NoSuchElementException] {
        queue.peekKey
      }
    }

    test(s"$impl: dequeueing empty queue throws exception") {
      assert(queue.isEmpty)
      intercept[NoSuchElementException] {
        queue.dequeue()
      }
    }

    test(s"$impl: enqueue odds and evens items and verify") {
      for (x ← odds) queue.enqueue(x, x)
      for (x ← evens) queue.enqueue(x, x)
      assert(!queue.isEmpty)
      assertResult(6)(queue.size)
      for (i <- 0 until queue.size) assertResult(i)(queue(i))
      for (item ← queue) {} //just dequeue
      assert(queue.isEmpty)
    }

  }

  for (impl <- maxPqImpl) {

 	  var queue: IndexPriorityQueue[Int] = impl

     test(s"$impl: dequeue items") {
      for (x ← odds) queue.enqueue(x, x)
      for (x ← evens) queue.enqueue(x, x)
      for (i ← 5 to 0 by -1) assertResult(i)(queue.dequeue())
      assert(queue.isEmpty)
    }

    test(s"$impl: peek item") {
      queue.enqueue(0, 1)
      queue.enqueue(1, 2)
      assertResult(1)(queue.peekIndex)
      assertResult(2)(queue.peekKey)
      assertResult(2)(queue.size)
    }

    test(s"$impl: update item") {
      queue(1) = 0
      assertResult(0)(queue.peekIndex)
      assertResult(1)(queue.peekKey)
      assertResult(2)(queue.size)
    }

}

  for (impl <- minPqImpl) {

 	  var queue: IndexPriorityQueue[Int] = impl

      test(s"$impl: dequeue items") {
      for (x ← odds) queue.enqueue(x, x)
      for (x ← evens) queue.enqueue(x, x)
      for (i ← 0 to 5) assertResult(i)(queue.dequeue())
      assert(queue.isEmpty)
    }

    test(s"$impl: peek item") {
      queue.enqueue(0, 1)
      queue.enqueue(1, 2)
      assertResult(0)(queue.peekIndex)
      assertResult(1)(queue.peekKey)
      assertResult(2)(queue.size)
    }

    test(s"$impl: update item") {
      queue(1) = 0
      assertResult(1)(queue.peekIndex)
      assertResult(0)(queue.peekKey)
      assertResult(2)(queue.size)
    }

  }

}