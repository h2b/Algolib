# Algolib -- A Scala Library of Essential Algorithms

This is a Scala library of essential algorithms.

It starts with Scala implementations of some algorithms from the book *Algorithms* 4 by Robert Sedgewick and Kevin Wayne [1]. Further adaptations from this book will be added from time to time. Other algorithms may go into this library as well.

Currently, we have some fundamental abstract data types (ADT) like stack, queue and bag with different implementations, a bunch of static sorting algorithms and searching classes. The sorting package also has a comparison application suitable for performance measuring.

## API

See [Scaladoc](http://hans-hermann-bode.de/sites/default/files/sites/algolib/scaladocs/de/h2b/scala/lib/index.html).

## Maven Coordinates

See [The Central Repository](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22de.h2b.scala.lib%22%20AND%20%22algolib%22). Choose the version you want (of course, the latest one is recommended) and look under "Dependency Information".

## Licence

Algolib - A Scala Library of Essential Algorithms
  
Copyright 2015-2016 Hans-Hermann Bode
  
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see
[http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

## References

[1] R. Sedgewick, K. Wayne, [Algorithms, 4th edition](http://algs4.cs.princeton.edu) (Addison-Wesley Professional, 2011, ISBN 0-321-57351-X)
