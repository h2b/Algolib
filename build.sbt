name := "Algolib"

version := "0.1.1-SNAPSHOT"

scalaVersion := "2.12.0"

libraryDependencies ++= Seq(
	"de.h2b.scala.lib" %% "utilib" % "0.3.0",
	"org.scalatest" %% "scalatest" % "3.0.0" % "test",
	"junit" % "junit" % "4.12" % "test"
)