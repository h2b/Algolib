organization := "de.h2b.scala.lib"

organizationName := "private"

organizationHomepage := Some(url("http://h2b.de"))

homepage := Some(url("http://h2b.de"))

startYear := Some(2015)

description := """This is a Scala library of essential algorithms.

It starts with a Scala adaptation of some algorithms from the book Algorithms 4 
by R. Sedgewick and K. Wayne. Further adaptations from this book will be added 
from time to time. Other algorithms may go into this library as well.
"""

licenses := Seq("GNU General Public License, Version 3" -> url("https://www.gnu.org/licenses/gpl-3.0"))

pomExtra := Seq(
	<scm>
		<url>scm:git:https://gitlab.com/h2b/Algolib.git</url>
	</scm>,
	<developers>
		<developer>
			<id>h2b</id>
			<name>Hans-Hermann Bode</name>
			<email>projekte@h2b.de</email>
			<url>http://h2b.de</url>
			<roles>
				<role>Owner</role>
				<role>Architect</role>
				<role>Developer</role>
			</roles>
			<timezone>Europe/Berlin</timezone>
		</developer>
	</developers>
)
